### Office Suite - Word Save

* Source From Jenka_968 - Word workload
* Save/SaveAs documents

Prerequisite:
For Creation of new project,
1) Install Commandline parser, ILMerge.MSBuild.Tasks via Nuget manager or Package Manager
2) Include Reference libraries: Word COM library , System.Management, System.Web.Extensions

For existing sln file,
1) Open sln file
2) Build the sln file in Release mode.
3) Get Command line arguments details by running,
   
   WordSave.exe default
		or
   WordSave 0.0.0.1

Copyright c  2021


  -i, --InputFileName     Required. Absolute path of Input filename or Relative path (In Current directory only)

  -s, --ThreadSleep       (Default: 1000) Thread Sleep time
	
  -p, --IterationPause    (Default: 1000) Pause time between each iterations

  -o, --OutputFileName    Required. Absolute path of Output filename or Relative path (In Current directory only)

  -n, --Iterations        Required. Number of iterations
	  
  -f, --SaveExternal	  Required. To Save the document in an external file

  --StartupPause             (Default: 2000) Sleep time between application start and workload execution

  --Display                  (Default: 2) Display fullscreen setting: 1 (Full Screen) or 2 (custom window)

  --DisplayHeight            (Default: 700) Display screen height

  --DisplayWidth             (Default: 1200) Display screen width

  -r, --runs                 (Default: 1) Number to times run the application

  -v, --verbose              (Default: true) Turn on verbose logging

  -V, --scriptversion        (Default: 1.00) Wrapper script version

  -a, --on-measure-start     (Default: true) Blocking command to execute before the measurement period. Ideally should
                             exclude Initialization.

  -b, --on-measure-stop      (Default: true) Blocking command to execute after the measurement period. Ideally should
                             exclude deinitialization.

  -R, --results-directory    (Default: ..\output) Path to directory to storeWrapper result fileWrapper log
                             fileApplication raw resultsApplication log files

  --help                     Display this help screen.

  --version                  Display version information.