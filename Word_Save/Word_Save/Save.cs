﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;
using System.IO;

using CommandLine;
using Word = Microsoft.Office.Interop.Word;

using Helper;

namespace Workload
{
    public class Options : CommonOptions
    {
        public const bool inputFileFlag = true;
        public const bool outputFileFlag = true;
    }

    class SpecificArguments
    {
        // Command line option for Save
        public static Options ParseArgument(string[] args, Logger.Settings set)
        {
            Options options = new Options();

            if (args.Length != 0)
            {
                Utility.SetCaseID(args, set);

                // Invoke Save default
                if (args[0] == "default")
                {
                    options.InputFileName = "..\\input\\source_document.docx";
                    options.OutputFileName = "SaveOutput_Default.docx";
                    options.Iterations = 2;
                }
            }
            Arguments.ParseArgument(args, ref options);
            return options;
        }
    }

    static class Save
    {
        static void CreateCSV(Options opt, Logger.LogData logData, string status)
        {
            string operationname = logData.Benchmark.name;
            string startTime = logData.StartTime.time;
            string sysPath = Directory.GetCurrentDirectory();
            string csvfile = sysPath + "\\" + operationname + "_" + startTime + ".csv";

            string[] lines = File.ReadAllLines(csvfile);
            string[] InputFileName = opt.InputFileName.Split(',');

            if (lines.Length == 0)
            {
                throw new InvalidOperationException("The file is empty");
            }

            //add new column to the header row
            lines[0] += ",InputFileName";

            //add new column value for each row.
            for (int i = 1; i < lines.Length; i++)
            {
                lines[i] += "," + Path.GetFileName(InputFileName[i - 1]);
            }
            //write the new content
            File.WriteAllLines(csvfile, lines);
            Logger.LogProgramEnd(status, opt);
        }

        static void ValidateIterationValues(out string[] InputFileName, Options opt, Logger.LogData logData)
        {
            InputFileName = opt.InputFileName.Split(',');

            if(opt.Iterations != -1 && opt.Iterations == InputFileName.Length)
            {
                return;
            }
            else if(opt.Iterations == -1 || opt.Iterations < InputFileName.Length)
            {
                opt.Iterations = InputFileName.Length;
                return;
            }

            string[] modinputfilename = new string[opt.Iterations];

            for(int iter = 0; iter < opt.Iterations; iter++)
            {
                modinputfilename[iter] = InputFileName[(iter % InputFileName.Length)];
            }
            InputFileName = modinputfilename;
            opt.InputFileName = string.Join(",", InputFileName);
        }

        // Load Document
        static void WordSave(Word.Application app, List<Word._Document> documents, List<Word.Window> docWindows, Logger.Settings set,
             Options opt, Logger.LogData logData, int repeat)
        {
            for (int rep = 1; rep <= repeat; rep++)
            {
                string[] InputFileName = null;
                string operationName = "Save_" + rep.ToString();
                set.repetition = rep;

                ValidateIterationValues(out InputFileName, opt, logData);
                Utility.ValidateOutputFiles(rep, opt, logData);

                try
                {
                    set.iterationTimings.Add(operationName, new List<double>());

                    for (int iter = 0; iter < opt.Iterations; iter++)
                    {
                        set.fileNames.Add(InputFileName[iter]);
                        Utility.WordInit(out app, out documents, out docWindows, operationName, set, opt, logData);
                        Thread.Sleep(opt.SeparationPause);

                        string OutputFileName = Utility.GetFileName(opt.OutputFileName, rep, iter, InputFileName[iter]);
                        var watch = new System.Diagnostics.Stopwatch();

                        Logger.IterationEventStart(operationName, rep, iter);
                        watch.Start();

                        if (string.IsNullOrEmpty(opt.OutputFileName))
                        {
                            documents[0].Save();
                        }
                        else
                        {
                            documents[0].SaveAs2(FileName: OutputFileName, FileFormat: Word.WdSaveFormat.wdFormatDocumentDefault);
                        }
                        watch.Stop();
                        Logger.IterationEventEnd(operationName, rep, iter);
                        Thread.Sleep(opt.IterationPause);

                        set.iterationTimings[operationName].Add(watch.ElapsedMilliseconds / 1000.0);
                        Utility.IterationEnd(operationName, iter, logData, set, opt.IterationPause);

                        Thread.Sleep(opt.SeparationPause);
                        Utility.WordDeInit(app, documents, set, opt.SeparationPause);
                        set.fileNames = new List<string>();
                    }
                    set.status = "success";
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception raised. Program failed");
                    Console.WriteLine(e);
                    Console.WriteLine(e.Message);

                    Utility.WordDeInit(app, documents, set, opt.SeparationPause);
                    set.exception = e;
                    set.status = "failure";
                    Thread.Sleep(opt.SeparationPause);
                    return;
                }
                app = null;
                documents = null;
                docWindows = null;
            }
            Thread.Sleep(opt.SeparationPause);
            Logger.DeInit(logData, set, set.status, opt, set.exception);
            CreateCSV(opt, logData, set.status);
        }

        /// The main entry point for the application.
        [STAThread]
        static void Main(string[] args)
        {
            Options opt;

            Logger.Settings set = new Logger.Settings();
            opt = SpecificArguments.ParseArgument(args, set);

            if (args.Length == 0 || opt.InputFileName == null)
            {
                return;
            }

            string benchMarkTestName = "Word_Save";
            Logger.LogData logData = Logger.Init(opt, benchMarkTestName, "Word");

            Utility.ValidateInputFiles(opt, logData);

            // Decalre the Word objects
            Word.Application app = null;
            List<Word._Document> documents = null;
            List<Word.Window> docWindows = null;

            WordSave(app, documents, docWindows, set, opt, logData, (opt.runs));
        }
    }
}
